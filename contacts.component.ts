import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';

import { ContactsService,
         Contact,
         ApiKeyService,
         UserService,
         JwtService,
         ModalService,
         TableService} from '../../../../shared';

@Component({
    selector: '[app-contacts-panel]',
    templateUrl: './contacts.component.html',
    host: {'class': 'someClass1'} // tslint:disable-line
})
export class ContactsComponent implements OnInit, AfterViewInit {
    subscription: Subscription;
    contacts: Contact[] = [] as Contact[];
    updatingContact: Contact = {} as Contact;
    contactsViewConfig: any[];
    meta: any;
    importedContactsStats: any = {};
    uploader: FileUploader = new FileUploader({
        url: 'https://service.e-materials.com/api/import_contacts',
        removeAfterUpload: true,
        autoUpload: true,
        authToken: 'Bearer ' + this.jwtService.getToken(),
        headers: [{
            name: 'Api-Key',
            value: '' + this.apiKeyService.getApiKey()
        }]
    });
    progressBar: boolean = false;
    showErrors: boolean = false;
    editContactFormView: boolean = false;
    private query: any = {
        per_page: 10,
        page: 1
    };

    constructor(
        private contactsService: ContactsService,
        private userService: UserService,
        private jwtService: JwtService,
        private apiKeyService: ApiKeyService,
        private modalService: ModalService,
        private changeDetector: ChangeDetectorRef,
        private tableService: TableService
    ) {}

    ngOnInit() {
        this.tableService.onEditAction.subscribe((contact: Contact) => {
            this.openModal('edit-contact', contact);
        });

        this.tableService.onDeleteAction.subscribe((contact: Contact) => {
            this.remove(contact);
        });

        this.uploader.onAfterAddingFile = (file: any) => { file.withCredentials = false; };
        this.uploader.onBuildItemForm = (fileItem, form) => {
            form.append('xls_template', fileItem._file);
            return {fileItem, form};
          };

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            this.progressBar = false;
            this.uploader.clearQueue();
            this.importedContactsStats = JSON.parse(response);
            this.modalService.open('import-stats');
            this.runQuery();
        };

        this.uploader.onBeforeUploadItem = () => {
            this.progressBar = true;
        };
    }

    ngAfterViewInit() {
        this.runQuery();
        this.contactsViewConfig = this.contactsService.getConfig();
    }

    runQuery(query?) {
        this.query = query ? {...this.query, ...query} : this.query;
        this.contactsService.list(this.query).subscribe(
            (contacts: any) => {
                this.contacts = contacts.data;
                this.meta = contacts.meta;
            }
        );
    }

    remove(contact: Contact) {
        this.contactsService.delete(contact.id).subscribe(
            () => {
                this.runQuery();
            }
        );
    }

    openModal(id: string, contact?: Contact) {
        this.editContactFormView = false;
        this.updatingContact = contact;
        this.modalService.open(id);
        this.changeDetector.detectChanges();
        this.editContactFormView = true;
    }

    closeModal(id: string) {
        this.modalService.close(id);
    }

}
