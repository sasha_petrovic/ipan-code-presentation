import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseHttpService } from './baseHttp.service';
import { ContactSerializer } from '../serializers/contact.serializer';
import { PaginationSerializer } from '../components/pagination/pagination.serializer';
import { Contact } from '../models';

const contactsConfigView = [
    {
        headerName: 'Email',
        fieldValue: 'email',
    },
    {
        headerName: 'First Name',
        fieldValue: 'first_name',
    },
    {
        headerName: 'Last name',
        fieldValue: 'last_name',
    },
    {
        headerName: 'Phone',
        fieldValue: 'phone',
    },
    {
        headerName: 'Country',
        fieldValue: 'country',
    },
    {
        headerName: 'City',
        fieldValue: 'city',
    },
    {
        headerName: 'Address',
        fieldValue: 'address',
    },
    {
        headerName: 'Organization',
        fieldValue: 'organization',
    },
];

@Injectable()
export class ContactsService extends BaseHttpService<Contact> {
    constructor(httpClient: HttpClient) {
        super(
            httpClient,
            'contacts',
            new ContactSerializer(),
            new PaginationSerializer()
        );
    }

    getConfig() {
        return contactsConfigView;
    }
}
