import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { Serializer } from '../serializers/serializer';

export abstract class BaseHttpService<Model> {
    constructor(
        private http: HttpClient,
        private path: string,
        private modelSerializer: Serializer,
        private metaSerializer?: Serializer,
        private meta?: string,
        private explanation?: string
    ) {}

    private metaName = this.meta || 'pagination';

    private formatErrors(error: any) {
        return new ErrorObservable(error.error);
    }

    private convertData(data: any): Model[] {
        return data.map(item => {
            return this.modelSerializer.fromJson(item);
        });
    }

    private convertMeta(data: any): any[] {
        return data.map(item => this.metaSerializer.fromJson(item));
    }

    list(params: any): Observable<any> {
        return this.http.get(`${environment.api_url}${this.path}`, { params })
            .pipe(
                map((response: any) => {
                    const collectionData = {
                        data: this.convertData(response.data),
                        meta: !response.meta ? '' : !this.metaSerializer ? response.meta :
                        Array.isArray(response.meta[this.metaName]) ?
                            this.convertMeta(response.meta[this.metaName]) :
                            this.metaSerializer.fromJson(response.meta[this.metaName])
                    };
                    return collectionData;
                }),
                catchError(this.formatErrors));
    }

    get(id: number, params: any): Observable<Model> {
        return this.http.get(`${environment.api_url}${this.path}/${id}`, { params })
            .pipe(
                map((response: any) => {
                    return this.modelSerializer.fromJson(response.data);
                }),
                catchError(this.formatErrors));
    }

    create(model: Model, deepPath?: string, params?: any): Observable<Model> {
        console.log(params);
        return this.http.post(
            `${environment.api_url}${this.path + (deepPath ? deepPath : '')}`,
            JSON.stringify(model),
            { params }
        ).pipe(
            map((response: any) => {
                return this.modelSerializer.fromJson(response.data);
            }),
            catchError(this.formatErrors));
    }

    put(model: Model, id: number, params?: any): Observable<Model> {
        return this.http.put(
            `${environment.api_url}${this.path}/${id}`,
            JSON.stringify(model),
            { params }
        ).pipe(
            map((response: any) => {
                return this.modelSerializer.fromJson(response.data);
            }),
            catchError(this.formatErrors));
    }

    save(model: Model, id?: number, params?: any): Observable<Model> {
        return id ? this.put(model, id, params) : this.create(model, null, params);
    }

    delete(id: number | string): Observable<any> {
        return this.http.delete(
            `${environment.api_url}${this.path}/${id}`
        ).pipe(catchError(this.formatErrors));
    }

}
